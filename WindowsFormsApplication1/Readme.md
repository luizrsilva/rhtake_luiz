Bem vindo ao Projeto Correio San Andreas.

� necess�rio informar dois arquivos, um com os trechos v�lidos e outro com as encomendas solicitadas.
O sistema solicitar� uma pasta para salvar o arquivo rotas.txt com as melhores rotas calculadas.
Caso ocorra erro durante a execu��o, verifique o formato dos arquivos fornecidos e as permiss�es do usu�rio em alterar/salvar arquivos na pasta informada.

formato arquivos exemplo:

trechos.txt:
LS SF 1
SF LS 2
LS LV 1
LV LS 1
SF LV 2
LV SF 2
LS RC 1
RC LS 2
SF WS 1
WS SF 2
LV BC 1
BC LV 1
 
encomendas.txt:
SF WS
LS BC
WS BC