﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{

    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        string line;
        string pasta = "";
        List<string> trechos = new List<string>();
        List<string> encomendas = new List<string>();

        private void button1_Click(object sender, EventArgs e)
        {
            //Validar arquivos e diretorios
            if (lblTrechos.Text == "")
            {
                MessageBox.Show("Favor abrir o arquivo Trechos.txt.", "Atenção",
                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                button2.PerformClick();
                return;
            }
            if (lblEncomendas.Text == "")
            {
                MessageBox.Show("Favor abrir o arquivo Encomendas.txt.", "Atenção",
                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                button3.PerformClick();
                return;
            }
            if (pasta == "")
            {
                MessageBox.Show("Selecione um destino para salvar o arquivo Rotas.txt.", "Atenção",
                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                button4.PerformClick();
                return;
            }

            List<string> saida = new List<string>();
            CorreioRotas teste = new CorreioRotas(trechos, encomendas);

            //Calcula melhores rotas
            saida = teste.CalculaRota();

            // Gerando arquivo rotas.txt
            System.IO.StreamWriter outputFile = new System.IO.StreamWriter(@pasta);
            for (int i = 0; i < saida.Count(); i++)
                outputFile.WriteLine(saida[i]);

            outputFile.Flush();
            outputFile.Close();
            MessageBox.Show("Arquivo gerado com sucesso!", "",
            MessageBoxButtons.OK, MessageBoxIcon.Information);

        }
        
        public void button2_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                System.IO.StreamReader file = new System.IO.StreamReader(openFileDialog1.FileName);
                while ((line = file.ReadLine()) != null)
                {
                    trechos.Add(line);
                }
                lblTrechos.Text = openFileDialog1.FileName;
                file.Close();
            }
        }
        
        private void button3_Click(object sender, EventArgs e)
        {
            if (openFileDialog2.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                System.IO.StreamReader file = new System.IO.StreamReader(openFileDialog2.FileName);
                while ((line = file.ReadLine()) != null)
                {
                    encomendas.Add(line);
                }
                lblEncomendas.Text = openFileDialog2.FileName;
                file.Close();
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.Description = "Selecione uma pasta para salvar o arquivo rotas.txt.";
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                pasta = folderBrowserDialog1.SelectedPath + "\\rotas.txt";
                lblPasta.Text = folderBrowserDialog1.SelectedPath;
            }
            
        }
    }
}
