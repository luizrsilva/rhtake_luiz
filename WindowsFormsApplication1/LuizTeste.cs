﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    public class Trecho
    {
        public string cidade1, cidade2;
        public int dias;
    }

    public class Encomenda
    {
        public string origem, destino;
    }

    public class TesteLuiz
    {
        Trecho[] trechos;
        Encomenda[] encomendas;
        private List<string> rotas = new List<string>();
        private List<string> rotas2 = new List<string>();


        //Carrega os objetos encomendas e trechos
        public TesteLuiz(List<string> Trechos, List<string> Encomendas)
        {
            int a = 0;
            string[] linha;
            trechos = new Trecho[Trechos.Count];
            encomendas = new Encomenda[Encomendas.Count];

            while (a < Trechos.Count)
            {
                linha = Trechos[a].Split(' ');
                trechos[a] = new Trecho();
                trechos[a].cidade1 = linha[0];
                trechos[a].cidade2 = linha[1];
                trechos[a].dias = Convert.ToInt32(linha[2]);
                ++a;
            }
            a = 0;
            while (a < Encomendas.Count)
            {
                linha = Encomendas[a].Split(' ');
                encomendas[a] = new Encomenda();
                encomendas[a].origem = linha[0];
                encomendas[a].destino = linha[1];
                ++a;
            }
        }

        public static bool Luiz()
        {
            return true;

        }

        //Chama a função de calcular o trajeto, uma encomenda de cada vez
        internal List<string> CalculaRota()
        {
            int i = 0;
            while (i < encomendas.Count())
            {
                ProcuraRota(encomendas[i].origem, encomendas[i].destino);
                ++i;
            }

            return rotas;
        }

        string rota;
        int count, menorRotaDias;
        string menorRota;
        internal void ProcuraRota(string origem, string destino)
        {
            count = 0;
            menorRotaDias = 0;
            rota = origem;

            MontaRota(origem, destino, trechos);

            rotas.Add(menorRota);
                
        }
        internal void MontaRota(string o, string d, Trecho[] t)
        {

            for (int i = 0; i < t.Count(); i++)
            {
                if ((t[i].cidade1 == o) &&(rota.IndexOf(t[i].cidade2) == -1))
                {
                    rota = rota + " " + t[i].cidade2;
                    count += t[i].dias;
                    if (t[i].cidade2 == d)
                    {
                        if ((menorRotaDias==0)||(count<menorRotaDias))
                        {
                            menorRota = rota + " " + count.ToString();
                            menorRotaDias = count;
                        }
                        rota = rota.Remove(rota.Length - 3);
                        count += -t[i].dias;
                        return;
                    }
                    else
                    {
                        MontaRota(t[i].cidade2, d, t);
                        rota = rota.Remove(rota.Length - 3);
                        count += -t[i].dias;
                    }
                }
            }

            return;
        }
    }
}

